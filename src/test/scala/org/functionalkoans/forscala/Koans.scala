package org.functionalkoans.forscala

import org.scalatest._
import support.Master

class Koans extends Sequential(
    new AboutAsserts,
    new AboutValAndVar,
//    new AboutLiteralBooleans, // Skip it
//    new AboutLiteralNumbers, // Skip it
//    new AboutLiteralStrings, // Skip it
    new AboutMethods,
    new AboutClasses,
    new AboutUniformAccessPrinciple,
    new AboutConstructors,
    new AboutParentClasses,
    new AboutOptions, //class #3
    new AboutObjects, //class #3
    new AboutApply, //class #3
    new AboutTuples, //class #3
    new AboutHigherOrderFunctions, // class #5
    new AboutEmptyValues, // class #5
    new AboutLists, // class #5
    new AboutMaps, // class #5
    new AboutSets, // class #5
//    new AboutFormatting, // Skip it
    new AboutStringInterpolation, // class #9
    new AboutPatternMatching, // class #7
    new AboutCaseClasses, //class #4
    new AboutRange, // class #9
    new AboutPartiallyAppliedFunctions, // class #9
    new AboutPartialFunctions, // class #4
    new AboutImplicits, // class #8
    new AboutTraits, // class #8
    new AboutForExpressions, // class #6
    new AboutInfixPrefixAndPostfixOperators, // class #8
    new AboutInfixTypes, // class #8
    new AboutMutableMaps, // class #6
    new AboutMutableSets, // class #6
    new AboutSequencesAndArrays, // class #6
    new AboutIterables, //class #4
    new AboutTraversables, // class #4
    new AboutNamedAndDefaultArguments, // class #9
    new AboutTypeTags, // class #9
    new AboutPreconditions, // class #9
    new AboutExtractors, // class #7
    new AboutByNameParameter, // class #9
    new AboutRepeatedParameters, // class #9
    new AboutTypeSignatures, // class #9
    new AboutTypeVariance, // class #9
    new AboutEnumerations // class #7
)
