package pl.p.lodz.bootstrap

object Zajecia05 {
  def main(args: Array[String]) {

    //Tablice
    val numbers = Array(1,2,3)
    numbers.map(_ + 1)

    //Tuple
    val pair = (1, "One")
    println(pair._1)
    println(pair._2)

    val pair2 = Tuple2(1, "One")
    val pair3 = 2 -> "Two"

    val(v1, v2) = pair2
    val(_, v3) = pair2
    pair2.productIterator.foreach(println)


  }
}

