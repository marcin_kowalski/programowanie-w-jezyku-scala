package pl.p.lodz.bootstrap

class Exclamation(s: String) {
  override def toString = s + "!"
}

object Exclamation {
  def apply(s: String) = new Exclamation(s)
}
