package pl.p.lodz.bootstrap

//#1
object S {}

class Zajecia03 {

  def main(args: Array[String]) {

//    new S
//
//    //#2
//    val x = 1
//    var y : Int = 2
//
//    y = 3
//    x = 2
//
//    //#3
//    var z : Int = "5"
//
//    //#4
//    val t1 = Array[String](1,2,3)
//
//    //#5
//    println(t1)
  }

  //#6
  def max(x: Int, y: Int): Int = {
    if (x > y) {
      return x
    } else {
      return y
    }
  }

  //#7
  def gcd(a: Int,b: Int): Int =  {
    if(b == 0) {
      return a
    } else {
      return gcd(b, a%b)
    }
  }
}
